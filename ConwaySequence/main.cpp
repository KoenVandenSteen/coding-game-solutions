#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

int main()
{
	int R;
	cin >> R; cin.ignore();
	int L;
	cin >> L; cin.ignore();


	string output = to_string(R);

	for (int i = 1; i < L; ++i)
	{
		string temp = "";

		string curNumber;;
		bool start = true;
		int counter = 0;

		istringstream iss(output);
		string s;

		while (getline(iss, s, ' ')) {


			if (start) {
				curNumber = s;
				start = false;
			}

			if (curNumber != s)
			{
				temp += to_string(counter) + " ";
				temp += curNumber + " ";
				curNumber = s;
				counter = 1;
			}
			else
			{
				++counter;
			}
		}

		temp += to_string(counter) + " " + curNumber;
		output = temp;

	}

	cout << output;
}