#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	vector<string> grid;

	int width; // the number of cells on the X axis
	cin >> width; cin.ignore();

	int height; // the number of cells on the Y axis
	cin >> height; cin.ignore();


	for (int i = 0; i < height; i++) {
		string line; // width characters, each either 0 or .
		getline(cin, line);


		grid.push_back(line);
		cerr << "Input: " << line << endl;
	}

	for (int y = 0; y < grid.size(); ++y)
	{
		for (int x = 0; x < grid.at(y).size(); ++x)
		{

			if (grid.at(y).at(x) != '.') {

				string answerPos, answerX, answerY;

				answerPos = to_string(x) + " " + to_string(y) + " ";

				cerr << answerPos << endl;

				for (int neighbourX = x + 1; neighbourX < grid.at(y).size() + 1; ++neighbourX)
				{
					if (neighbourX < grid.at(y).size() && grid.at(y).at(neighbourX) == '0')
					{
						answerX = to_string(neighbourX) + " " + to_string(y) + " ";
						cerr << " link found: " << answerX << endl;
						break;
					}
					else
					{
						answerX = "-1 -1 ";
						cerr << " no link found: " << answerX << endl;
					}
				}


				for (int neighbourY = y + 1; neighbourY < grid.size() + 1; ++neighbourY)
				{
					if (neighbourY < grid.size() && grid.at(neighbourY).at(x) == '0')
					{
						answerY = to_string(x) + " " + to_string(neighbourY) + " ";
						cerr << " link found: " << answerY << endl;
						break;
					}
					else
					{
						answerY = "-1 -1 ";
						cerr << "no link found: " << answerY << endl;
					}
				}

				cout << answerPos << answerX << answerY << endl;
			}
		}


	}

}