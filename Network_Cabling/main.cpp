#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;


int main()
{
	int N;
	cin >> N; cin.ignore();

	bool firstTime = true;

	int startingX = 0;
	int furthestX = 0;

	int averageY = 0;
	vector<int> Yvalues;

	for (int i = 0; i < N; ++i) {

		int X;
		int Y;

		cin >> X >> Y; cin.ignore();

		if (firstTime)
		{
			startingX = X;
			furthestX = X;
			firstTime = false;
		}

		Yvalues.push_back(Y);

		if (startingX > X) {
			startingX = X;
		}

		if (furthestX < X) {
			furthestX = X;
		}
	}

	sort(Yvalues.begin(), Yvalues.end());

	averageY = Yvalues.at(Yvalues.size() / 2.f);

	long long CableLength = 0;


	for (int x = 0; x < Yvalues.size(); ++x) {
		CableLength += abs(Yvalues.at(x) - averageY);
	}


	CableLength += abs(startingX - furthestX);
	if (Yvalues.size() == 1)
		cout << 0;
	else
		cout << CableLength << endl;
}