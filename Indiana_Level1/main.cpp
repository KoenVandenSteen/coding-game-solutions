#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <fstream>

using namespace std;


int main()
{
	int W; // number of columns.
	int H; // number of rows.
	cin >> W >> H; cin.ignore();

	vector<vector<int>> tunnelGrid;

	tunnelGrid.resize(H);

	for (int i = 0; i < H; i++) {
		string LINE; // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
		getline(cin, LINE);

		istringstream lineStream(LINE);
		string coordinate;

		while (getline(lineStream, coordinate, ' '))
		{
			int coord = atoi(coordinate.c_str());
			tunnelGrid.at(i).push_back(coord);
		}

	}

	int EX; // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
	cin >> EX; cin.ignore();

	vector<string> Tiles;

	///putting all the different tiles in a vector for easy acces;

	//LEFT TOP RIGHT

	//type0
	Tiles.push_back("0 0 0 0 0 0");

	//type1
	Tiles.push_back("0 1 0 1 0 1");

	//type2
	Tiles.push_back("1 0 0 0 2 0");

	//type3
	Tiles.push_back("0 0 0 1 0 0");

	//type4
	Tiles.push_back("0 0 2 0 0 1");

	//type5
	Tiles.push_back("0 1 1 0 0 0");

	//type6
	Tiles.push_back("1 0 0 0 2 0");

	//type7
	Tiles.push_back("0 0 0 1 0 1");

	//type8
	Tiles.push_back("0 1 0 0 0 1");

	//type9
	Tiles.push_back("0 1 0 1 0 0");

	//type10
	Tiles.push_back("0 0 2 0 0 0");

	//type11
	Tiles.push_back("0 0 1 0 0 0");

	//type12
	Tiles.push_back("0 0 0 0 0 1");

	//type13
	Tiles.push_back("0 1 0 0 0 0");


	// game loop
	while (1) {
		int XI;
		int YI;
		string POS;

		cin >> XI >> YI >> POS; cin.ignore();


		int piece = tunnelGrid.at(YI).at(XI);
		int moveX = 0;
		int moveY = 0;

		cerr << "piece: " << piece << endl;

		if (POS == "LEFT")
		{
			moveX = atoi(Tiles.at(piece).substr(0, 1).c_str());
			moveY = atoi(Tiles.at(piece).substr(2, 1).c_str());

			cerr << "move to: " << moveX << " " << moveY << endl;
		}

		if (POS == "TOP")
		{
			moveX = atoi(Tiles.at(piece).substr(4, 1).c_str());
			moveY = atoi(Tiles.at(piece).substr(6, 1).c_str());

			cerr << "move to: " << moveX << " " << moveY << endl;
		}


		if (POS == "RIGHT")
		{
			moveX = atoi(Tiles.at(piece).substr(8, 1).c_str());
			moveY = atoi(Tiles.at(piece).substr(10, 1).c_str());

			cerr << "move to: " << moveX << " " << moveY << endl;
		}

		if (moveX == 2)
			moveX = -1;
		if (moveY == 2)
			moveY = -1;

		XI += moveX;
		YI += moveY;

		cerr << endl;

		cout << XI << " " << YI << endl; // One line containing the X Y coordinates of the room in which you believe Indy will be on the next turn.
	}
}