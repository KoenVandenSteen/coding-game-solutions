#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int nbFloors; // number of floors
	int width; // width of the area
	int nbRounds; // maximum number of rounds
	int exitFloor; // floor on which the exit is found
	int exitPos; // position of the exit on its floor
	int nbTotalClones; // number of generated clones
	int nbAdditionalElevators; // ignore (always zero)
	int nbElevators; // number of elevators

	vector<int> Elevators;

	cin >> nbFloors >> width >> nbRounds >> exitFloor >> exitPos >> nbTotalClones >> nbAdditionalElevators >> nbElevators; cin.ignore();

	Elevators.resize(nbFloors);

	for (int i = 0; i < nbElevators; i++) {
		int elevatorFloor; // floor on which this elevator is found
		int elevatorPos; // position of the elevator on its floor
		cin >> elevatorFloor >> elevatorPos; cin.ignore();

		Elevators.at(elevatorFloor) = elevatorPos;

	}

	// game loop
	while (1) {
		int cloneFloor; // floor of the leading clone
		int clonePos; // position of the leading clone on its floor
		string direction; // direction of the leading clone: LEFT or RIGHT
		cin >> cloneFloor >> clonePos >> direction; cin.ignore();


		cerr << to_string(clonePos) << endl;
		cerr << width << endl;

		if (direction == "RIGHT" && cloneFloor != exitFloor &&  Elevators.at(cloneFloor) >= clonePos)
			cout << "WAIT" << endl;

		else if (direction == "LEFT" && cloneFloor != exitFloor && Elevators.at(cloneFloor) <= clonePos)
			cout << "WAIT" << endl;

		else if (direction == "RIGHT" && cloneFloor == exitFloor && exitPos >= clonePos)
			cout << "WAIT" << endl;
		else if (direction == "LEFT" && cloneFloor == exitFloor && exitPos <= clonePos)
			cout << "WAIT" << endl;
		else
			cout << "BLOCK" << endl;
	}
}