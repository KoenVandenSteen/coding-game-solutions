#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int N; // the total number of nodes in the level, including the gateways
	int L; // the number of links
	int E; // the number of exit gateways
	vector <vector<int>> Links;
	vector <int> Exits;
	cin >> N >> L >> E; cin.ignore();

	Links.resize(L*L);

	for (int i = 0; i < L; i++) {
		int N1; // N1 and N2 defines a link between these nodes
		int N2;
		cin >> N1 >> N2; cin.ignore();

		Links.at(N1).push_back(N2);
		Links.at(N2).push_back(N1);
		cerr << "position: " << N1 << " move: " << N2 << endl;
	}



	for (int i = 0; i < E; i++) {
		int EI; // the index of a gateway node
		cin >> EI; cin.ignore();
		Exits.push_back(EI);
	}

	// game loop
	while (1) {
		int SI; // The index of the node on which the Skynet agent is positioned this turn
		cin >> SI; cin.ignore();

		string answer = "";
		vector<int>::iterator deletedIterator;

		for (vector<int>::iterator it = Links.at(SI).begin(); it != Links.at(SI).end(); ++it)
		{
			int i = *it;

			if (std::find(Exits.begin(), Exits.end(), i) != Exits.end()) //exit found in the next list;
			{
				answer = to_string(SI) + " " + to_string(i);
				deletedIterator = it;
				break;
			}
		}

		if (answer != "") {
			Links.at(SI).erase(deletedIterator);
		}
		else
		{
			answer = to_string(SI) + " " + to_string(*Links.at(SI).begin());
			Links.at(SI).erase(Links.at(SI).begin());
		}

		cout << answer << endl; // Example: 0 1 are the indices of the nodes you wish to sever the link between

	}
}