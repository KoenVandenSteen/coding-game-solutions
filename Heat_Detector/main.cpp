#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;


int main()
{
	int W; // width of the building.
	int H; // height of the building.
	cin >> W >> H; cin.ignore();
	int N; // maximum number of turns before game over.
	cin >> N; cin.ignore();



	int X0;
	int Y0;
	cin >> X0 >> Y0; cin.ignore();
	int curX = X0;
	int curY = Y0;

	int X1 = 0, Y1 = 0;
	int X2 = W, Y2 = H;



	cerr << "input done";

	while (1) {


		cerr << endl;
		cerr << endl;

		string BOMB_DIR; // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
		cin >> BOMB_DIR; cin.ignore();



		if (BOMB_DIR == "U")
		{
			X1 = curX;
			Y1 = Y1;
			X2 = curX;
			Y2 = curY;
			curX = curX;
			curY = curY - (abs(Y1 - Y2) / 2.0f);
		}

		if (BOMB_DIR == "UR")
		{
			X1 = curX;
			Y1 = Y1;
			X2 = X2;
			Y2 = curY;
			curX = curX + (abs(X1 - X2) / 2.0f);
			curY = curY - (abs(Y1 - Y2) / 2.0f);
		}

		if (BOMB_DIR == "R")
		{
			X1 = curX;
			Y1 = curY;
			X2 = X2;
			Y2 = curY;
			curX = curX + (abs(X1 - X2) / 2.0f);
			curY = curY;
		}

		if (BOMB_DIR == "DR")
		{
			X1 = curX;
			Y1 = curY;
			X2 = X2;
			Y2 = Y2;
			curX = curX + (abs(X1 - X2) / 2.0f);
			curY = curY + (abs(Y1 - Y2) / 2.0f);
		}

		if (BOMB_DIR == "D")
		{
			X1 = curX;
			Y1 = curY;
			X2 = curX;
			Y2 = Y2;
			curX = curX;
			curY = curY + (abs(Y1 - Y2) / 2.0f);
		}

		if (BOMB_DIR == "DL")
		{
			X1 = X1;
			Y1 = curY;
			X2 = curX;
			Y2 = Y2;
			curX = curX - (abs(X1 - X2) / 2.0f);
			curY = curY + (abs(Y1 - Y2) / 2.0f);
		}

		if (BOMB_DIR == "L")
		{
			X1 = X1;
			Y1 = curY;
			X2 = curX;
			Y2 = curY;
			curX = curX - (abs(X1 - X2) / 2.0f);
			curY = curY;
		}

		if (BOMB_DIR == "UL")
		{
			X1 = X1;
			Y1 = Y1;
			X2 = curX;
			Y2 = curY;
			curX = curX - (abs(X1 - X2) / 2.0f);
			curY = curY - (abs(Y1 - Y2) / 2.0f);
		}


		cout << curX << " " << curY << endl;

	}
}
